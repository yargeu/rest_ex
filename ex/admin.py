from django.contrib import admin
from .models import TestUser, Score

admin.site.register(TestUser)
admin.site.register(Score)