Aaron	아론	산에 사는 사람, 성경의 등장인물로 모세의 형이며 인류 최초의 제사장. 		
Abel	아벨	생명력		
Abigail	아비가엘	Father of Joy 즐거운		
Abraham	에이브러햄	군중 열국의 아버지 		
Ace	에이스	최고의 것		
Ada	아다	번영하고 즐거운, 여자이름		
Adam	아담	The earth 흙, 땅, 남자 기독교 성서에서 하느님이 처음으로 창조한 남자 		
Adela	아델	평화로운 존귀한 우아한		
Adelio	아델리오	고결한 스페인 왕자의 아버지		
Adolph	아돌프	늑대		
Adonis	아도니스	여신 아프로디테가 사랑한 미소년		
Adora	아도라	아주 사랑하는		
Agatha	아가타	나무랄 데 없는 정숙한 여인, 여자이름		
Aggie	애기	훌륭한		
Aida	아이다	베르디 오페라의 주인공인 에티오피아 왕녀		
Ailish	앨리쉬	엘리자베스의 모습을 한		
Aimee	애이미	사랑하는 프랑스 친구		
Alan, Allan	앨런	사냥개, 조화 잘생긴		
Albert	앨버트	빅토리아 여왕의 부군, 남자이름 대단히 뛰어난		
Albino	앨비노	피부색소가 결핍된 사람 		
Alex	알렉스	인류의 수호자, 남자이름		
Alexa	from Alexandria: Protector 보호자 			
Alexander	알렉산더	Protector그리스인의 수호자, 보호자,조력자 		
Alexandra	알렉산드라	그리스인의 수호자 여자		
Alexia	알렉시아	돕다 		
Alexis	아렉시스	Helpful 도움을 주는		
Alfred	앨프레드, 알프레드	평화 그 자체, 강자, 조언자 앨프레드 대왕, 남자이름		
Ali	알리	숭고한, 이슬람 4대 칼리프 		
Alice	앨리스	진실, 여자이름 		
Alicia	알리시아	Truthful 진실한 		
Alika	앨리카	진실한 하와이인 		
Allie	앨리	순결한 탄생 		
Allison	앨리슨	Of noble birth 고귀한 태생		
Aloha	알로하	사랑스러운 		
Alvin	앨빈	고귀한 친구 		
Alyssa	앨리사	TruthNoble 고귀한		
Amanda	아만다	Worthy of love 사랑 받을 만한 , 여자이름 		
Amber	앰버	Amber colored 호박색의, 영롱한		
Ami	애미	친구 		
Amos	아모스	무거운 짐, 남자이름 		
Amy	에이미	가장 사랑하는 사람,귀여운 		
Anais	아나이스	은혜로운 		
Andra	안드라	강하고 용감한 		
Andrea	 앤드리아	Courageous 용기있는 여인 		
Andrew	앤드류	Courageous 용기있는 영국인 		
Andy	앤디	강하고 남자다운, 남자이름 Andrew의 애칭		
Angel	엔젤	천사, 귀여운 아이 		
Angela	앤젤라	천사,소식을 알리는 사람 		
Angelica	안젤리카	켈리포니아산 백포도주 		
Anika	애니카	매우 아름다운 		
Ann	앤	우아한,인자한 		
Anna	안나	Graceful 우아함으로 가득 찬, 여자이름 		
Annie	애니	여자이름 		
Anthony	안토니	worthy of praise 찬양받는, 칭찬할 만한 가치 		
Antonio	안토니오	worthy of praise 찬양받는 		
Apollo	아폴로	옛 그리스 로마의 태양신, 굉장한 미남자 		
Aria	아리아	오페라 등에서 악기의 반주가 있는 독창곡 		
Ariel	아리엘	중세 전설의 공기의 요정 		
Arista	아리스타	최고의 그리스인 		
Arnold	아놀드	독수리처럼 강한, 명예를 중시하는, 남자이름 		
Arthur	아더, 아서	뛰어난 사람, 곰 		
Arvid	아비드	폴란드 독수리 		
Asha	아샤	생명 		
Ashley	애쉴리	From the Ash tree 물푸레나무의 요정, 요정같은		
Aster	아스터	영국 별 		
Astin	아스틴	별모양의 		
Athena	아테네	그리스 신화 중 지헤와 전쟁의 여신 		
Audrey	오드리	현귀한 사람 		
Aurora	오로라	새벽 동틀 녘, 새벽의 여신		
Austin, Augustin	어스틴, 어거스틴	Revered, Exalted, 빼어난 		
Autumn	어텀	Fall 가을의 		
Ava	아바	새 같은 		
Baba	바바	화요일에 태어난 		
Bailey	베일리	성벽으로 둘러쌓인 안뜰 청지기 (성으로도 쓰임)		
Baldy	발디	대머리 		
Bambi	밤비	오스트리아 작가 Felix Salten의 동물소설 주인공인 아기사슴 		
Barbara	바바라	다른 지방의 사람,타향에서 온 사람, 낯선, 여자이름		
Barbie	바비	금발의 플라스틱 인형, 전형적인 미국인 		
Barley	발리	보리, 대맥 		
Barney	바니	언쟁, 남자이름		
Baron	바론	독일의 귀족 		
Basil	바실	위엄있는, 장엄한 		
Baxter	박스터	존경할 만한 		
Beau	보	프랑스어로 아름다운 		
Bebe	베베	아기 		
Beck	벡	시내 강물 		
Becky	베키	마음을 사로잡는 것, 여자이름 		
Belita	벨리타	아름다운 		
Bella	벨라	아름다운 여자이름		
Belle	벨	불어로 아름다움 		
Benecia	베네치아	라틴어로 축복받은 		
Benedict	베네딕트	축복받은 		
Benjamin	벤자민	행운아 		
Benny	베니	가장 사랑하는 어린 아들, 남자이름 		
Berg	버그	독일어로 산 		
Bernice	버니스	승리의 소식을 가져온 사람 		
Beryl	베릴	행운 		
Bess	베스	신의 서약 		
Bessie	베시	신성함, 여자이름 		
Betty	베티	신의 서약 (Elizabeth의 준말)		
Biana	비안나	숨기고 있는 		
Bianca	비앙카	순백의 		
Bibiane	비비안	살아있는 		
Billy	빌리	경찰봉, 남자이름 		
Bingo	빙고	게임의 일종, 야단 법석 		
Bishop	비숍	목자 		
Blanche	블란치	하얗게 말쑥하고 아름다운,백인족,순결한 		
Bliss	블리스	다시 없는 기쁨 		
Blondie	블론디	금발의 여자 		
Bonita	보니타	예쁜 		
Bonnie	바니	곱고,아름답고,우아하고 상냥한 사람 		
Bono	보노	모두가 훌륭한 		
Boris	보리스	슬라브어로 전사 		
Boss	보스	두목 		
Breanna	브리인나	Strong one 강한 자 		
Brenda	브렌더	검,검은 머리털 		
Brian	Bryan	Strong one, Brave 강한 사람 용기있는 		
Briana	브리아나	Virtuous 정숙한 		
Brianna	브리안느	Fearless 용기있는		
Bright	브라이트	빛나는, 생기있는 		
Brittany	브리트니	British 영국인의		
Brooke	부르크	Brook 시냇물 		
Bruno	브루노	갈색 머리 		
Buck	벅	사슴 		
Bunny	버니	작은 토끼 친구		
Caesar	시저	로마 황제 		
Caley	칼리	용감한 전사 		
Calix	칼릭스	매우 잘생긴 		
Calla	칼라	아름다움 		
Callia	칼리아	프랑스어로 아름다운 		
Camilla	카밀라	로마신화에 등장하는 여걸, 여자이름 		
Candace	캔디스	정열적,착실한,순결한 		
Captain	캡틴	우두머리 		
Cara	카라	값이 비싼 물건 친구 친애하는 사람		
Carmel	카멜	히브리어로 정원		
Carmen	카르멘	아름다운 목소리를 가진 사람, 여자이름 		
Caroline	캐로라인	Melody 가벼운, 경쾌한 		
Caroline	캐럴린	용감한,건강한,튼튼한 		
Casey	캐시	용감하고 경계심 많은 		
Cassandra	카산드라	She who is ignored 아무도 몰라주는,  고독한 		
Cassidy	캐시디	Clever 현명한 		
Catherine	캐서린	순결한 사람 		
Cathy	캐시	순결한 사람 		
Cecil	세실	남,녀 모두, 장님 		
Celestyn	셀레스틴	폴란드어로 하늘 		
Celina	셀리나	달		
Cha Cha	샤샤	남아메리카에서 유래된 볼룸댄스 		
Champ	챔프	챔피언 		
Charles	찰스	남자다운		
Charles	찰스	Manly, 남자다운 		
Charlie	찰리	백인 		
Charlotte	샬럿	몸이 건강하고 여성적인 사람 		
Chase	체이스	사냥꾼 		
Chavi	샤비	여자 아이 		
Chelsea	첼시아	런던의 옛 區이름 		
Cherie	쉐리	사랑받는 		
Cherry	체리	인자하고 얼굴이 앵두처럼 붉은 사람 		
Chilli	칠리	칠레고추로 만든 향신료 		
Chloe	클로에	Blooming 활짝 핀 여자이름, 전원시에 나오는 양치는 소녀의 이름 		
Chrissy	크리시	기독교 신자 		
Christina 	크리스티나	기독교 신자 		
Christine	크리스틴	그리스의 신도 		
Christopher	크리스토퍼	 예수를 따르는 사람들 		
Chubby	처비	토실토실 살찐, 얼굴이 통통한 		
Cindy	신디	신데렐라에서 유래 		
Clara	클라라	깨끗한, 성직자		
Claude	클라우드	뛰어난 		
Claudia	클라우디아	여자이름 		
Cleo	클레오	찬양하다, 여자이름 		
Cleta	클레타	침침한 		
Cliff	클리프	낭떠러지 		
Coco	코코	코코야자, 사람의 머리		
Cody	코디	쿠션, 방석		
Colin	콜린	강하고 씩씩한		
Connie	코니	견고하고 변하지 않는, 여자이름		
Conrad	콘라드	적절한 조언자 		
Cookie	쿠키	작고 납작한 케이크, 매력적인 여자, 귀여운 소녀		
Corby	코비	까마귀같이 어두운 		
Courtney	코트니	Bold 용기있는		
Coy	코이	수줍어하는 		
Coyote	코요테	북미 대초원에 사는 늑대, 망나니		
Crimson	크림슨	진홍색의 		
Crispin	크리스핀	곱슬 머리 		
Crystal	크리스탈	깨끗한 		
Cutie	큐티	귀여운 여자 		
Cyclone	사이클론	큰 회오리 바람 		
Cyma	시마	그리스어로 번영하다. 		
Daisy	데이지	아주 좋은 물건, 프랑스 국화, 여자이름		
Dali	달리	스페인 초현실주의 화가 		
Dana	다나	신의 어머니,총명하고 순결한 사람 		
Daniel	다니엘	God is my judge 신은 나의 심판자 		
Danielle	데니엘	God is my judge 신실한		
Danika	다니카	금성 		
Darby	다비	자유로운 남자		
Daria	다리아	여왕 같은 		
Darin	다린	값진 선물 		
Dario	다리오	유복한		
Darlene	달린	온유하고 귀엽다,자상하게 돌보는 사랑 		
Darwin	다윈	사랑하는 친구		
Dave	데이브	가장 사랑하는 사람, 용감한 남자, 남자이름 		
David	데이비드	Beloved 사랑받는 용감한 이스라엘 제2대왕인 다윗을 일컬음, 남자		
Dean	딘	지도자 		
Della	델라	고귀한 		
Delling	델링	불꽃을 튀기다. 		
Delphine	델핀	프랑스어로 꽃으로부터, 여자이름 		
Denise	더니즈	꽃의 대표 		
Dennis	데니스	와인 애호가, 남자이름 		
Denver	덴버	푸른 계곡 		
Derry	데리	고대 아일랜드어로 빨간 머리 		
Destiny	데스티니	One"s fate 운명적인		
Deva	데바	신성한 정신 		
Dexter	덱스터	솜씨가 좋은 		
Diallo	디알로	아프리카어 굵은 		
Diana	다이아나	Divine, 신성한		
Dick	딕	강력한 		
Dino	디노	단검 		
Dixie	딕시	큰냄비, 미 남부 여러 주의 별명 		
Donald	도널드	용감한자 		
Donna	돈나	숙녀 		
Doreen	도린	신의 선물 		
Doris	도리스	그리스의 도리스 지방, 바다에서 온, 해양의 여신, 여자이름 		
Dorothy	도로시	신의 선물 		
Douglas	더글러스	짙은 회색 검은 언덕		
Duke	듀크	공작, 남자이름 		
Duncan	던컨	족장 		
Dustin	더스틴	용맹스런 전사 		
Dyllis	딜리스	꾸밈없는 		
Eavan	에반	고대 아일랜드어로 정의로운 이 		
Ebony	에보니	인도산 흑단, 흑단처럼 새까만 		
Echo	에코	메아리 		
Edan	에단	고대 아일랜드어로 불 		
Edeline	에델린	높은 신분으로 태어난 		
Eden	에덴	히브리어로 평원 		
Eden	이든	낙토,낙원,극락 		
Edgar	에드가	행복을 만드는자 		
Edith	에디스	전쟁,전투 		
Edmund	에드문드	행복을 지키는자 		
Edward	에드워드	행복한 인도자 행복의 옹호자		
Edwin	에드윈	행복한 정복자 성공한 친구		
Edwina	에드위너	가치 있는 친구,재산을 받는 사람 		
Eileen	아이린	밝은,귀여운,사랑스러운 		
Eilis	엘리스	고대 아일랜드어로 친절한 		
Eldora	엘도라	스페인어로 황금 		
Elf	엘프	꼬마요정 		
Elin	엘린	행복한 		
Elisha	엘리샤	히브리어로 신의 구원자 		
Elizabeth	엘리자베스	신을 위해 봉헌하다는 의미, 신의 서약, 신실한 여자이름 		
Ella	엘러	횃불 		
Elle	엘르	여성 		
Ellen	엘렌	횃불 		
Elroy	엘로이	프랑스어로 왕 		
Elsa	엘사	고귀한 것, 여자이름		
Elva	엘버	신기하고 지혜로운 		
Elvis	엘비스	현명한 왕자 		
Elysia	엘리시아	축복받은 집 		
Emily	에밀리	Worker열심히 일하는 자		
Emma	Emilia	Ambitious 야망있는		
Enoch	에녹	신에게 바쳐진, 성경 중 에녹서라는게 있음. 금서긴 하지만.. 		
Eric	에릭	Honorable ruler 강력한 통치자, 영예로운 지도자 남자이름 		
Erica	에리카	Honorable ruler 영예로운 지도자 위대한 지도자 		
Erin	에린	Ireland 아일랜드 인 		
Eris	에리스	질투의 여신 		
Eros	에로스	사랑 		
Esteban	에스테반	승리의 왕관 		
Esther	에스더	별 		
Ethan	이쓴	Strong 강한 		
Eugene	유진	명문의 		
Eva	에바	여자이름 		
Evan	에반	젊은 용사 		
Eve	이브	하느님이 창조한 최초의 여자, 여자이름 		
Evelyn	이벨린	남,녀 개암나무 열매 		
Faith	페이스	Faith 믿음		
Fanny	패니	자유로운 사람 		
Farrell	파렐	고대 아일랜드어로 용맹스러운 		
Favian	파비앙	용감한 남자 		
Fedora	페도라	그리스어로 신성한 선물 		
Felice	펠리체	이탈리아어로 행복한 		
Felix	펠릭스	운이 좋은, 남자이름 		
Fella	펠라	Fellow의 의미 		
Ferdianand	페르디난드	이해가 빠른 사람, 평화 		
Fidelio	피델리오	이탈리아어로 성실한 		
Filia	필리아	그리스어로 친분 		
Fleta	플레타	쾌속의 		
Flora	플로라	꽃,꽃의 신 		
Florence	플로렌스	번영하는 		
Floria	플로리아	꽃이 만발한 		
Forrest	포레스트	숲 남자		
Frederick	프레데릭	독일계 힘센 옹호자 		
Freeman	프리맨	자유인 		
Gabriel	게이브리엘	하나님은 나의 힘		
Gabriella	가브리엘 	God is my strength 신실한		
Gabrielle	가브리엘	하나님은 바로 힘이다. 		
Gali	갈리	히브리어로 저수지 		
Gem	젬	보석		
Gemma	제마	보석 		
Geoffrey	조프리	신의 평화, 강력한 보호자 		
George	조지	남자이름, 영국 왕의 이름 농부를 뜻하기도 함		
Georgia	조지아	농부 		
Geraldine	제럴딘	강하고 힘있는 창 		
Gilbert	길버트	금처럼 빛나는 		
Gili	길리	기뻐하다.		
Giovanni	지오반니	신의 영광		
Gladys	글래디스	공주 		
Gloria	글로리아	영광을 주는 사람,영예가 되는 사람 		
Goofy	구피	바보 같은, '미키마우스'의 개 이름 		
Grace	그레이스	Grace of God 신실한		
Grania	그라니아	곡식의 여신		
Gregory	그레고리	신념이 굳은 		
Gregory	그레고리	야경꾼		
Hailey	헤일리	Hero 영웅		
Haley	할리	영웅 		
Halona	할로나	운좋은 		
Hannah	해나	Grace 신의 은총, 하나님의 은총으로 아이를 낳은 사무엘의 어머니		
Happy	해피	행복한 		
Harace	헤레이스	눈부시게 아름다운 		
Harley	할리	일류 의사들의 동네		
Harmony	하모니	아름다운 조화 		
Harold	해럴드	용감한 자, 승리자 통치자		
Harriet	해리어트	가정주부 		
Harry	해리	약탈하다, 괴롭히다. 남자이름 		
Hazel	헤이즐	영수,지휘관 		
Heba	헤바	아라비아어 선물을 주다 		
Hedy	헤디	달콤,사람으로 하여금 감상할수 있는것 		
Helen	헬렌	횃불,밝은 		
Helia	헬리아	그리스어로 태양 		
Heloise	헬로이즈	건전한 		
Henry	헨리, 앙리	프랑스 가장 		
Hera	헤라	하늘의 여왕 		
Hermosa	허모서	아름답다 		
Hero	히어로	영웅 		
Hestia	헤스티아	가정이 여신 		
Hilda	힐더	전투,여자 병사 		
Hollis	홀리스	영웅		
Honey	허니	귀여운 사람 		
Honey	허니	벌꿀, 멋진 것, 훌륭한 것		
Hope	호프	희망, 소망 		
Hubert	휴버트	깨끗한 마음		
Hue	휴	베트남어로 꽃 		
Huey	휴이	'도널드 덕'에서의 조카 중 한 명 		
Hugh	휴	이상의, 정신적인 높은 		
Humphery	험프리	평화의 옹호자 		
Ian	이안	존이라는 이름에서 파생 		
Ida	아이더	즐거운,부지런한,부유한 		
Iliana	일리아나	그리스어로 밝은 		
Indira	인디라	천둥 번개의 신 		
Ingrid	잉그리드	딸,귀여운 사람, 목초지		
Irene	아이린	평화,평화의 여신 		
Irina	이리나	슬라브어로 평화로운 		
Iris	아이리스	그리스어로 무지개 		
Isaac	이삭	Abraham과 Sarah의 아들이며 Jacob의 아버지 		
Isabel	이사벨	스페인어로 신을 위해 바치다.		
Isabel	이자벨	하느님의 서약 		
Isadora	이사도라	여자이름 		
Isis	이시스	그리스어로 가장 강력한 이집트 여신 		
Issac	이삭	히브리, 영어식 웃음 성경에서 본 기억이.. 으읔 		
Ivy	아이비	그리스 신화 중에 신성한 음식물 		
Jace	제이스	매력적인 		
Jack	잭	사나이, 버릇 없는 놈 		
Jackson	잭슨	미국 제7대 대통령, 남자이름 		
Jaclyn	재클린	보호하다 		
Jacob	야곱	히브리, 자콥, 영어식 함정을 파는자 비겁한 놈이라 해도 됨 		
Jacqueline	재클린	To protect 수호하는 		
Jade	제이드	Green gem 녹색 옥돌, 에메랄드 		
Jade	제이드	녹색 보석 		
James	Supplanter	빼앗는 자 		
Jamie	제이미	대신하는 자 		
Jane	제인	하느님은 자비하다,소녀,처녀 		
Janet	제니트	소녀,신의 은총 		
Janice	재니스	처녀,하느님은 인자하다. 		
Jasmine	재즈민	Happy 꽃, 기쁜, 행복한, 인도원산의 상록관목		
Jasper	제스퍼	벽옥, 남자이름 		
Jefferson	제퍼슨	미국 제 3대 대통령		
Jeffrey	제프리	평화의 선물		
Jenifer	제니퍼	웨일즈어로 하얀 물결 white wave		
Jenna	제나	Small bird 작은 새 		
Jennie	제니	파도 		
Jennifer	제니퍼	흰 파도,마술 요법,요정,매혹적인 여자 		
Jenny	제니	처녀,소녀 		
Jeremy	제레미	기원전 6, 7세기의 대예언자		
Jericho	제리코	페르시아어로 달의 도시		
Jerome	제롬	성스러운 법, 성스러운 이름 		
Jerry	제리	라틴어로 성스러운 		
Jess	제스	남자이름		
Jessica	제시카	Peace 평화, 신의 은총, 부를 뜻하기도 함		
Jessie	제시	부유한 사람, 여자이름 		
Jocelyn	자슬린	유쾌한,즐거운 		
Jodie	조디	찬양하다 		
Joe	조	연인 		
Johanna	조안나	신의 은혜 		
John, jack	존	God is merciful 신은 자비로우시다		
Jolly	졸리	즐거운, 유쾌한 		
Jonathan	조나단	God is merciful 신은 자비로우시다		
Jordan	조르단	히브리어로 전해오는 		
Joy	조이	기쁨 		
Juan	유안	God is merciful 신은 자비로우시다 		
Jud	쥬드	기도하다 		
Judith	주디스	찬미,얌전한 여자 		
Julia	쥴리아	Youthful 풋풋한 라틴어로 젊은 		
Juliana	쥴리아나	부드러운 머리결의 		
Julie	줄리	온유하고 얌전한 얼굴 		
Juliet	쥴리엣	'로미오와 쥴리엣'의 여자 주인공 		
Justin	져스틴	진실		
Kaitlyn	케이틀린	Pure 순수한		
Kali	칼리	어둠의 여신 		
Kama	카머	사랑의 신 		
Kara	카라	달콤한 멜로디 		
Karen	카런	순결하다 		
Karena	카레나	순수한 것 		
Karis	카리스	그리스어로 은혜로운		
Kassia	카시아	폴란드어로 순수한 		
Kate	케이트	켈트어로 처녀스런 		
Katelyn	케이틀린	Pure 순수한		
Katherine	캐서린	Pure 순수한 Virginal 순결한		
Kathryn	캐스린	Virginal 순결한		
Katie	케이티	Virginal 순결한  		
Kayla	케일러	Joyful 기쁜		
Kaylee	캐일리	#NAME?		
Kellan	켈란	강력한		
Kelley	켈리	여전사		
Kelsey	켈시	Island of the ships 양들의 쉼터 		
Kenneth	케니스	온화한 사람 		
Kerri	케리	신비스러운		
Kevin	케빈	남자이름		
Kiara	키아라	Soft 부드러운 		
Kimberley	킴벌리	황실의 잔디에서 태어난 사람 		
Kimberly	킴벌리	A diamond filled rock 귀한 		
Kitty	키티	순결한, 작은 고양이		
Klaus	클라우스	독일어로 승리의 인도자 		
Kori	코리	그리스어로 소녀 		
Kuper	쿠퍼	히브리어로 구리		
Kyle	카일	Handsome잘생긴 		
Kylie	카일리	Wood 숲의 		
Kyra	키라	숙녀 같은 		
Lakia	라키아	아랍어로 재산 		
Lala	랄라	튤립 		
Lamis	라미스	아랍어로 부드러운 		
Lani	라니	하와이어로 하늘 		
Lappy	래피	무릎에 앉기를 좋아하는 사람 		
Lara	라라	유명한 		
Laura	로라	Beautiful 아름다운 월계수, 승리		
Lauren	로렌	Artistic 예술적인		
Lavina	라비나	라틴어로 로마의 여인 		
Lawrence	로렌스	월계관을 쓴, 뛰어난 전사라는 의미도 있음. 		
Lee	리	초원 (초원에 사는 사람)		
Leena	리나	조명 		
Leila	릴라	검은 머리카락의 미녀,밤에서 태어난 사람 		
Lelia	렐리아	그리스어로 옳은 말 		
Leo	레오	사자, Leopard		
Leonard	레오나드	사자처럼 힘센 남자 		
Leopold	레오폴드	독일어로 사랑스런 사람		
Leslie	레슬리	작은 목장 		
Lev	레브	히브리어로 마음 		
Lewis, Louis	루이스	뛰어난 전사, 위의 로렌스의 애칭으로도 쓰임. 		
Lidia	리디아	폴란드어로 아시아에 있는 성 		
Lillian	릴리언	백합꽃 한송이,순결의 상징,신의 서약 		
Lily	릴리	나리, 백합, 순백한 것 		
Lina	리나	이탈리아어로 빛 		
Linda	린다	예쁜 사람		
Lisa	리사	히브리어로 신에게 바치다		
Lloyd	로이드	회색의, 남자이름		
Lonnie	로니	잘생긴 사람		
Lottie	로티	여자다운, 여성에게 어울리는 		
Louis	루이스	프랑스 루이왕, 유명한 전사, 남자이름		
Lowell	로웰	프랑스어로 사랑받는		
Lucia	루시아	이탈리아어로 광채		
Lucifer	루시퍼	샛별, 금성		
Lucy	루시	광명과 지혜를 갖고 온 사람 		
Lukas	루카스	그리스어로 빛 		
Luna	루나	달의 여신 		
Mabel	메이블	온유한 사람, 친절하고 상냥한 사람 나의 아름다운 사람		
Mackenzie	매켄지	Son of Kenneth 케네스의 자손		
Madeline	매들린	High tower 높은 탑   		
Madison	매디슨	Son of Mathew 마태오의 자손		
Madonna	마돈나	스페인어로 나의 소녀 		
Mag	매그	진주 		
Maggie	매기	여자이름 		
Makaio	마카이오	하와이어로 신의 선물 		
Makayla	매카일라	Who is like God? 신실한 		
Malissa	맬리사	달콤한 꿀 		
Malo	말로	하와이어로 승리자 		
Mamie	메이미	바다의 여자 		
Mana	마나	정신적인 선물 		
Mandelina	만델리나	사랑스러운 		
Mandy	맨디	사랑스런 		
Manon	마농	프랑스 소설에 나오는 주인공 		
Marcia	마르시아	용기 		
Margaret	마가레트	라틴어로 진주 		
Maria	마리아	Mistress of the sea 바다신의 정부, 사랑스러운, 슬프고 고달픈		
Mariah	마리아	God is my teacher 신실한 		
Marissa	매리사	Of the sea 바다의 		
Mark	마크	war-happy 전쟁을 좋아하는 		
Martin	마틴	호전적인, 중세 기사의 수호성인. 		
Martina	마티나	전쟁의 여신 		
Mary	메어리	From Maria 성모마리아, 바다의 여자		
Mathilda	마틸다	힘 		
Matthew	매튜	gift of God, 신의 선물 		
Maya	마야	그리스어로 어머니 		
Megan	미건	Strong 강한		
Melina	멜리나	그리스어로 밝은 노랑 		
Melissa	멀리사	A honey bee 꿀벌, 부지런한 		
Meriel	메리엘	켈트어로 빛나는 바다 		
Michael	마이클	신과 닮은, 대천사 미카엘을 칭함. 		
Michaela 	미쉘라	Who is like God? 신실한 		
Michelle	미쉘	Who is like God? 신실한		
Mickey	미키	남자이름 		
Mighty	마이티	강력한, 힘센 		
Mikayla 	미카일라	God like 신실한		
Minnie	미니	여자이름 		
Miranda	미란다	라틴어로 칭찬해 줄 만한 		
Missy	미시	아가씨 		
Misty	미스티	안개가 짙은, 눈물어린		
Molly	몰리	여자이름, Mistress of the sea 바다의 정부, 사랑스러운		
Monet	모네	프랑스어로 고독한		
Monica	모니카	그리스어로 조언자 		
Morgan	몰간	Seashore 평화로운 해변		
Morris	모리스	남자이름 		
Muffin	머핀	둥근빵 모양의 케이크 		
Mulan	뮬란	중국어로 목련꽃 		
Murphy	머피	감자 		
Nadia	나디아	슬라브어로 희망에 찬 		
Nalo	날로	아프리카어로 사랑스러운 		
Nami	나미	일본어로 파도 		
Nana	나나	할머니, 유모 		
Nancy	낸시	우아하고 온유하다 		
Nani	나니	그리스어로 예의 바른 		
Naomi	나오미	유쾌한, 여자이름 		
Nara	나라	그리스어로 행복한		
Narcisse	나르시스	프랑스어로 수선화		
Natalie	내틀리	To be born 탄생의		
Navid	나비드	좋은 소식 		
Neal	닐	챔피온 		
Neema	니마	힌두어로 번영하는		
Nelly	넬리	횃불 		
Nero	네로	강력한 		
Nia	니아	챔피언		
Nicholas	니콜라스	Victorious, 승리의 성니콜라스, 남자이름		
Nicky	닉키	민중의 승리, 귀엽고 생기있는 캐릭터에 어울림 		
Nicola	니콜라	승리 		
Nicole	니콜	Victory 승리		
Nina	니나	9번째의, 여자이름 		
Noel	노엘	남,녀 크리스머스 		
Odelia	오델리아	신에게 맹세하다, 키가작은 부유한		
Olga	올가	슬라브어로 성스런, 신성한 평화		
Olive	올리브	평화의 상징, 올리브 열매 		
Oliver	올리버	, Olivia 올리비아, 올리브 나무 (여자) 		
Olivia	올리비아	Olive tree 올리브나무, 풍요로운, 평화자		
Oscar	오스카	남자이름, 신성한 힘, 뛰어난 행정관 아카데미상 수상자에게 주는 작은 황금상 		
Owen	오웬	젊은이 		
Pablo	파블로	작은		
Pag	패그	진주 		
Paige	페이지	Youthful assistant 활기찬 		
Paloma	팔로마	스페인어로 비둘기 		
Pamela	파멜라	그리스어로 연인 개구장이		
Pandora	팬도라	세상의 첫번째 여자 		
Patricia	패트리샤	출신이 고귀하고 기품이 있는		
Patrick패트릭	기품있는 귀족			
Paul	포올	small, 작은 		
Pavel	파벨	슬라브어로 작은		
Peggy	페기	진주, 여자이름		
Pello	펠로	그리스어로 돌 		
Penda	펜다	슬라브어로 사랑받는 		
Penny	페니	말없는 편직자 		
Peppi	페피	인내하다 		
Peter	피터	바위 		
Petra	페트라	돌 		
Phila	필라	사랑 		
Philip	필립	말을 좋아하는 사랑스런		
Philippa	필리퍼	말을 사랑하는 사람 		
Phillip	필립	사랑하는 것 		
Phoenix	피닉스	젊은 여자 		
Pinky	핑키	연분홍색의 		
Pluto	플루토	명왕성 		
Poco	포코	약간, 조금씩 스페인어의 step		
Polo	폴로	4명이 1조가 되어 말을 타고 하는 공치기, 의류 브랜드 		
Pooky	푸키	독일어로 귀여운 사람 		
Poppy	포피	꽃으로 부터 		
Primo	프리모	이탈리아어로 장남 		
Prince	프린스	왕자 		
Princess	프린세스	공주, 왕비 		
Puffy	퍼피	바람이 확부는, 살찐 		
Queena	퀴너	매우 고귀하고 귀족화 적이다 		
Rabia	라비아	아프리카어로 봄 		
Rachel	레이첼	Motherly 모성이 강한, 야곱의 둘째 부인		
Raina	레이나	평화로운 		
Ralph	랄프	늑대와 같은 조언자 		
Rambo	람보	혼자 사는 기술을 터득하고 폭력적에 보복하는 영화주인공 		
Rania	라니아	여왕 		
Ravi	라비	태양 		
Rebecca	레베카	To tie 결속시키는, 화합하는, 성서의 리브가-이삭의 아내		
Redford	레드포드	붉은 강 넘어 		
Reggie	레기	힘있는 통치자, 남자이름 		
Rei	레이	법, 규칙 		
Remy	레미	프랑스어로 여러신의 어머니로부터 유래, 프랑스 꼬냑 브랜드 		
Rex	렉스	왕 		
Richard	리처드	대단히 강한 		
Ricky	리키	부유하고 힘센 사람 		
Riley	라일리	A small stream 시냇물, 쾌활한 		
Ringo	링고	반지 		
Rio	리오	스페인어로 강 		
Risa	리사	웃음소리 		
Robbie	로비	빛나는 명성, 남자이름 (Robert의 애칭)		
Robert	로버트	밝은 명성,붉은 수염, 		
Robin	로빈	길들여진 새, 남자이름 		
Rocky	록키	바위가 많은, 바위 같은 		
Roja	로자	스페인어로 붉은 		
Roland	롤란드	영어, 롤랑 프랑스 시골 신사 		
Rollo	롤로	남자이름, Rudolph의 애칭 		
Romeo	로미오	'로미오와 쥴리엣' 의 남자주인공 		
Rosie	로지	장미, 여자이름 		
Roxy	록시	빛나는 새벽 		
Roy	로이	빨간머리털이 있는, 남자이름 		
Ruby	루비	귀중한 빨간 보석 		
Rudolph	루돌프	유명한 늑대, 남자이름 		
Rudy	루디	유명한 늑대, 남자이름 		
Ryan	리안	어린 왕 		
Sabrina	서브리나	Thorny cactus 선인장같은, 강인한 		
Sally	샐리	출격, 분출, 재담, 소풍 		
Salvatore	살바토르	이탈리아어로 구원자 		
Sam	샘	남자이름, 멋있는 사내 		
Samantha	사만다	The listener말을 잘 듣는 자, 겸손한 사람		
Samson	삼손	히브리어로 태양과 같이 밝은 		
Samuel	사무엘	신의 말을 알아듣는 		
Sandy	샌디	모래의, 엷은 갈색의. 스코틀랜드 사람의 별명 		
Sara	새라	Princess 공주, 고귀한, 열국의 어미		
Sarah	새라	Princess 공주, 고귀한, 열국의 어미		
Sasha	사샤	협력자, 동료, 배우자 		
Savannah	서바나	Treeless plain 광활한 		
Scarlet	스칼렛	타는 듯이 붉은 		
Scoop	스쿠프	국자, 일확천금 		
Sean	숀	God is merciful 신은 자비로우시다 		
Sebastian	세바스챤	남자이름 숭배받는		
Selina	셀리나	여자이름, 달 		
Selma	셀마	공평한 		
Serena	세레나	고요한 		
Severino	세브리노	엄중한 		
Shaina	샤이나	아름다운 		
Shasa	샤사	아프리카어로 귀중한 물 		
Shelby	쉘비	A sheltered town 피난처, 안식처		
Sheri	쉐리	친애하는 		
Sierra	시에라	Black 검은 색, 고요한  		
Silky	실키	명주의, 부드럽고 매끈매끈한 		
Simba	심바	아프리카 사자 		
Simon	사이먼	남자이름, 그리스도의 열 두 사도의 한사람 		
Sniper	스니퍼	도요새 사냥꾼 		
Solomon	솔로몬	기원전 10세기 이스라엘의 현왕 		
Sonia	소니아	지혜 		
Sonny	써니	젊은 남자, 소년 		
Sophia	소피아	Wisdom 지혜 		
Sophie	소피	지혜 		
Sora	소라	노래하는 새 		
Sparky	스파키	활발한, 발랄한, 생생한 		
Spooky	스푸키	잘 놀라는, 겁 많은 		
Spotty	스포티	반점이 많은, 얼룩덜룩한 		
Stella	스텔라	밝은 별, 여자이름 		
Stephanie	스테파니	Crown 왕관, 고귀한		
Steven	스티븐	crown 왕관, 남자이름 		
Sting	스팅	찌르다 		
Storm	스톰	폭풍우 		
Sugar	슈가	설탕 		
Sunny	써니	빛나는 		
Sweetie	스위티	기분좋은 사람, 애인 		
Sydney	시드니	Enchanting 매력적인		
Sylvester	실베스터	남자이름, 숲 		
Sylvia	실비아	여자이름 		
Talia	탈리아	이탈리아어로 아침 이슬 		
Talli	탈리	영웅 		
Tanesia	타네시아	월요일에 태어난 		
Tania	타니아	불꽃같은 여왕 		
Taylor	테일러	Tailor 재단사, 솜씨있는		
Ted	테드	성스런 선물 		
Teenie	티니	작은 사람 		
Terra	테라	이탈리아어로 대지 		
Tess	테스	여자이름 토마스 하디의 작품에 나오는 비운의 주인공		
Theodore	테오도르	신의 선물 		
Thomas	타머스	Twins, 쌍둥이 /영국병사		
Tomo	토모	지적인 		
Trisha	트리샤	귀부인 (Patricia의 약어)		
Trudy	트루디	사랑 받는 		
Tylor	타일러	Tiler 지붕이나 타일을 붙이는 사람		
Uba	우바	아프리카어로 귀족 		
Umberto	움베르토	이탈리아어로 대지의 색깔 		
Valencia	발렌시아	용감한 정신 		
Vanessa 	바네사	 Butterfly 나비같은		
Velika	벨리카	슬라브어로 거대한 		
Vera	베라	이태리어로 진실 		
Verdi	베르디	이탈리아의 가극작곡가 		
Veronica	베로니카	스페인어로 진리 		
Victoria	빅토리아	Victorious 승리하는		
Vincent	빈센트	정복자, 남자이름 		
Violet	바이올렛	제비꽃 		
Vito	비토	이탈리아어로 생명 		
Vivi	비비	이탈리아어로 살아있는 		
Vivian	비비안	웅덩이 남녀공용		
Waldo	왈도	통치자 		
Wallace	월라스	이국의 		
Walter	월터	강력한 영주 		
Walter	월터	힘있는 전사, 남자이름 		
Weenie	위니	프랑크푸르트 소시지, 장애물 		
Wendy	웬디	방랑자, 여자이름 		
William	윌리엄	Desire to protect 보호의 욕구, 영국의 왕		
Wily	윌리	꾀가 많은, 약삭빠른 		
Winston	윈스톤	남자이름 		
Woody	우디	수목이 우거진 		
Yaro	야로	아프리카어로 아들 		
Yeti	예티	티베트의 설인		
Yuki	유키	일본어로 눈snow		
Zaza	자자	히브리어로 이동 		
Zeki	제키	터키어로 영리한 		
Zelia	젤리아	그리스어로 열중 		
Zena	제나	공손한 사람 		
Zenia	제니아	인심좋은, 여자이름 		
Zenon	제논	이방인 		
Zeppelin	제플린	비행선 		
Zeus	제우스	올림푸스 산의 최고의 신 		
Zili	질리	나의 그림자 		
Zinna	지나	창조적인 		
Zizi	지지	헝가리어로 신성 		
Zoe	조우	여자이름, 프랑스의 연구용 원자로 		
Zorro	조로	만화주인공, 스페인령 캘리포니아에서 활약하는 검은 복면의 쾌걸 		
Zulu	줄루	남아프리카 공화국 Natal주에 사는 용맹한 종족, 사냥을 좋아하는 개 